var map = L.map('main_map').setView([25.452320, -100.974668], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attreibution: '&copy; <a href="https://ww.openstreetmap.org/copyright">OpenStreetMAp</a> contributors'
}).addTo(map);

L.marker([25.452320, -100.974668]).addTo(map);